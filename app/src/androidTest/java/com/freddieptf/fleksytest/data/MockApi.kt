package com.freddieptf.fleksytest.data

import com.freddieptf.fleksytest.data.api.PagedResponse
import com.freddieptf.fleksytest.data.api.TmDbApiService
import com.freddieptf.fleksytest.data.db.model.TvShow
import kotlin.math.ceil
import kotlin.math.min

class MockApi : TmDbApiService {

    val pageSize = DataRepository.PAGE_SIZE
    val topRatedShows = mutableListOf<TvShow>()
    val similarShows = mutableListOf<TvShow>()

    override suspend fun getTopRatedShows(page: Int): PagedResponse<TvShow> {
        val _page = page - 1
        return PagedResponse(
            page = page,
            results = if (topRatedShows.size < pageSize) topRatedShows else topRatedShows.subList(
                _page * pageSize,
                min((_page * pageSize) + pageSize, topRatedShows.size)
            ),
            totalPages = ceil(topRatedShows.size.toDouble() / pageSize.toDouble()).toInt(),
            totalResults = topRatedShows.size
        )
    }

    override suspend fun getSimilarShows(showId: Long, page: Int): PagedResponse<TvShow> {
        val _page = page - 1
        return PagedResponse(
            page = page,
            results = if (similarShows.size < pageSize) similarShows else similarShows.subList(
                _page * pageSize,
                min((_page * pageSize) + pageSize, similarShows.size)
            ),
            totalPages = ceil(similarShows.size.toDouble() / pageSize.toDouble()).toInt(),
            totalResults = similarShows.size
        )
    }

}