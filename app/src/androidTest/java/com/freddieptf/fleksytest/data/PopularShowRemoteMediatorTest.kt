package com.freddieptf.fleksytest.data

import androidx.paging.*
import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.freddieptf.fleksytest.data.db.TvShowDb
import com.freddieptf.fleksytest.data.db.model.PopularShow
import com.freddieptf.fleksytest.data.db.model.TvShow
import junit.framework.Assert.assertFalse
import junit.framework.Assert.assertTrue
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@OptIn(ExperimentalPagingApi::class)
class PopularShowRemoteMediatorTest {

    private lateinit var mediator: PopularShowRemoteMediator
    private lateinit var api: MockApi
    private lateinit var db: TvShowDb

    @Before
    fun setUp() {
        api = MockApi()
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        db = Room.inMemoryDatabaseBuilder(appContext, TvShowDb::class.java)
            .allowMainThreadQueries()
            .build()
        mediator = PopularShowRemoteMediator(api, db)
    }

    @After
    fun tearDown() {
        db.clearAllTables()
        api.topRatedShows.clear()
        api.similarShows.clear()
    }

    @Test
    fun testRefreshEndReached() = runBlocking {
        val shows: List<TvShow> = (1..20).map {
            TvShow(
                it.toLong(),
                "Show $it",
                voteAvg = 10.minus(it).toDouble(),
                imgUrl = null,
                overview = null,
                originalLanguage = "lang-$it"
            )
        }
        api.topRatedShows.addAll(shows)
        val pagingState =
            PagingState<Int, TvShow>(listOf(), null, PagingConfig(DataRepository.PAGE_SIZE), 0)
        val result = mediator.load(LoadType.REFRESH, pagingState)

        assertTrue(result is RemoteMediator.MediatorResult.Success)
        assertTrue((result as RemoteMediator.MediatorResult.Success).endOfPaginationReached)
    }

    @Test
    fun testRefreshReturnsTrueWhenMoreData() = runBlocking {
        val shows: List<TvShow> = (1..50).map {
            TvShow(
                it.toLong(),
                "Show $it",
                voteAvg = 10.minus(it).toDouble(),
                imgUrl = null,
                overview = null,
                originalLanguage = "lang-$it"
            )
        }
        api.topRatedShows.addAll(shows)
        val pagingState =
            PagingState<Int, TvShow>(listOf(), null, PagingConfig(DataRepository.PAGE_SIZE), 0)
        val result = mediator.load(LoadType.REFRESH, pagingState)

        assertTrue(result is RemoteMediator.MediatorResult.Success)
        assertFalse((result as RemoteMediator.MediatorResult.Success).endOfPaginationReached)
    }

    @Test
    fun testAppend() = runBlocking {
        val shows: List<TvShow> = (1..50).map {
            TvShow(
                it.toLong(),
                "Show $it",
                voteAvg = 10.minus(it).toDouble(),
                imgUrl = null,
                overview = null,
                originalLanguage = "lang-$it"
            )
        }
        api.topRatedShows.addAll(shows)
        db.showDao().insertAll(shows)
        db.showDao().insertPopular(shows.subList(0, 20).map { PopularShow(it.id, it.voteAvg, 1) })

        val pagingState =
            PagingState<Int, TvShow>(listOf(), null, PagingConfig(DataRepository.PAGE_SIZE), 0)

        var result = mediator.load(LoadType.APPEND, pagingState)
        assertTrue(result is RemoteMediator.MediatorResult.Success)
        assertFalse((result as RemoteMediator.MediatorResult.Success).endOfPaginationReached)

        result = mediator.load(LoadType.APPEND, pagingState)
        assertTrue(result is RemoteMediator.MediatorResult.Success)
        assertTrue((result as RemoteMediator.MediatorResult.Success).endOfPaginationReached)
    }

}