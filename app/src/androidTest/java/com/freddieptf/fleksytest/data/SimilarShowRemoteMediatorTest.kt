package com.freddieptf.fleksytest.data

import androidx.paging.*
import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.freddieptf.fleksytest.data.db.TvShowDb
import com.freddieptf.fleksytest.data.db.model.TvShow
import junit.framework.Assert
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@OptIn(ExperimentalPagingApi::class)
class SimilarShowRemoteMediatorTest {
    private lateinit var mediator: SimilarShowRemoteMediator
    private lateinit var api: MockApi
    private lateinit var db: TvShowDb

    @Before
    fun setUp() {
        api = MockApi()
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        db = Room.inMemoryDatabaseBuilder(appContext, TvShowDb::class.java)
            .allowMainThreadQueries()
            .build()
        mediator = SimilarShowRemoteMediator(1, api, db)
    }

    @After
    fun tearDown() {
        db.clearAllTables()
        api.topRatedShows.clear()
        api.similarShows.clear()
    }

    @Test
    fun testRefreshEndReached() = runBlocking {
        val shows: List<TvShow> = (1..50).map {
            TvShow(
                it.toLong(),
                "Show $it",
                voteAvg = 10.minus(it).toDouble(),
                imgUrl = null,
                overview = null,
                originalLanguage = "lang-$it"
            )
        }
        db.showDao().insertAll(shows)
        api.similarShows.addAll(shows.take(12))
        val pagingState =
            PagingState<Int, TvShow>(listOf(), null, PagingConfig(DataRepository.PAGE_SIZE), 0)
        val result = mediator.load(LoadType.REFRESH, pagingState)
        Assert.assertTrue(result is RemoteMediator.MediatorResult.Success)
        Assert.assertFalse((result as RemoteMediator.MediatorResult.Success).endOfPaginationReached)
    }

    @Test
    fun testRefreshMoreData() = runBlocking {
        val shows: List<TvShow> = (1..50).map {
            TvShow(
                it.toLong(),
                "Show $it",
                voteAvg = 10.minus(it).toDouble(),
                imgUrl = null,
                overview = null,
                originalLanguage = "lang-$it"
            )
        }
        db.showDao().insertAll(shows)
        api.similarShows.addAll(shows.take(21))
        val pagingState =
            PagingState<Int, TvShow>(listOf(), null, PagingConfig(DataRepository.PAGE_SIZE), 0)
        val result = mediator.load(LoadType.REFRESH, pagingState)
        Assert.assertTrue(result is RemoteMediator.MediatorResult.Success)
        Assert.assertFalse((result as RemoteMediator.MediatorResult.Success).endOfPaginationReached)
    }


}