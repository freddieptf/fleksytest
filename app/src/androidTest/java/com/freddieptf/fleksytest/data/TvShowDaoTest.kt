package com.freddieptf.fleksytest.data

import androidx.paging.PagingSource
import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.freddieptf.fleksytest.data.db.TvShowDb
import com.freddieptf.fleksytest.data.db.model.PopularShow
import com.freddieptf.fleksytest.data.db.model.SimilarShow
import com.freddieptf.fleksytest.data.db.model.TvShow
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class TvShowDaoTest {

    private lateinit var db: TvShowDb

    @Before
    fun setUp() {
        db = Room
            .inMemoryDatabaseBuilder(
                InstrumentationRegistry.getInstrumentation().context,
                TvShowDb::class.java
            )
            .allowMainThreadQueries()
            .build()
    }

    @After
    fun tearDown() {
        db.clearAllTables()
    }

    @Test
    fun testGetLowestLatestShowPage() = runBlocking {
        val shows: List<TvShow> = (1..50).map {
            TvShow(
                it.toLong(),
                "Show $it",
                voteAvg = 10.minus(it).toDouble(),
                imgUrl = null,
                overview = null,
                originalLanguage = "lang-$it"
            )
        }
        db.showDao().insertAll(shows)
        val popular = mutableListOf<PopularShow>()
        popular.addAll(shows.subList(0, 20).map { PopularShow(it.id, it.voteAvg, 1) })
        popular.addAll(shows.subList(21, 40).map { PopularShow(it.id, it.voteAvg, 2) })
        db.showDao().insertPopular(popular)

        assert(db.showDao().lowestRatedShowItem()!!.page == 2)
    }

    @Test
    fun testGetPopularShows() = runBlocking {
        val shows: List<TvShow> = (1..50).map {
            TvShow(
                it.toLong(),
                "Show $it",
                voteAvg = 10.minus(it).toDouble(),
                imgUrl = null,
                overview = null,
                originalLanguage = "lang-$it"
            )
        }
        db.showDao().insertAll(shows)
        val LOAD_SIZE = 10
        val popular = shows.subList(0, LOAD_SIZE).map { PopularShow(it.id, it.voteAvg, 1) }
        db.showDao().insertPopular(popular)

        val pagingSource = db.showDao().popularShows()
        val page = pagingSource.load(PagingSource.LoadParams.Refresh(key = null, LOAD_SIZE, false))
        assertEquals(
            PagingSource.LoadResult.Page(
                data = popular.mapIndexed { idx, show ->
                    TvShow(
                        id = show.showId,
                        voteAvg = show.voteAvg,
                        title = "Show ${idx.plus(1)}",
                        overview = null,
                        imgUrl = null,
                        originalLanguage = "lang-${idx.plus(1)}"
                    )
                },
                itemsBefore = 0,
                itemsAfter = 0,
                nextKey = null,
                prevKey = null
            ),
            page
        )
    }

    @Test
    fun testDeletePopularShows() = runBlocking {
        val shows: List<TvShow> = (1..50).map {
            TvShow(
                it.toLong(),
                "Show $it",
                voteAvg = 10.minus(it).toDouble(),
                imgUrl = null,
                overview = null,
                originalLanguage = "lang-$it"
            )
        }
        db.showDao().insertAll(shows)
        val LOAD_SIZE = 10
        val popular = shows.subList(0, LOAD_SIZE).map { PopularShow(it.id, it.voteAvg, 1) }
        db.showDao().insertPopular(popular)
        assert(db.showDao().deletePopular() == LOAD_SIZE)
    }

    @Test
    fun testGetSimilarShows() = runBlocking {
        val shows: List<TvShow> = (1..50).map {
            TvShow(
                it.toLong(),
                "Show $it",
                voteAvg = 0.0,
                imgUrl = null,
                overview = null,
                originalLanguage = "en"
            )
        }
        db.showDao().insertAll(shows)
        val LOAD_SIZE = 10
        val similar = shows.subList(0, LOAD_SIZE).map { SimilarShow(shows.last().id, it.id, 1) }
        db.showDao().insertSimilar(similar)

        val pagingSource = db.showDao().similarShows(shows.last().id, "en")
        val page = pagingSource.load(PagingSource.LoadParams.Refresh(key = null, LOAD_SIZE, false))

        assertEquals(
            PagingSource.LoadResult.Page(
                data = similar.mapIndexed { idx, show ->
                    TvShow(
                        id = show.similarShowId,
                        voteAvg = 0.0,
                        title = "Show ${idx.plus(1)}",
                        overview = null,
                        imgUrl = null,
                        originalLanguage = "en"
                    )
                },
                itemsBefore = 0,
                itemsAfter = 0,
                nextKey = null,
                prevKey = null
            ),
            page
        )
    }

    @Test
    fun testGetLatestSimilarShowPage() = runBlocking {
        val shows: List<TvShow> = (1..50).map {
            TvShow(
                it.toLong(),
                "Show $it",
                voteAvg = 0.0,
                imgUrl = null,
                overview = null,
                originalLanguage = "en"
            )
        }
        db.showDao().insertAll(shows)
        val similar = mutableListOf<SimilarShow>()

        val show = shows.last()

        similar.addAll(shows.subList(0, 10).map { SimilarShow(show.id, it.id, 1) })
        similar.addAll(shows.subList(11, 20).map { SimilarShow(show.id, it.id, 2) })
        similar.addAll(shows.subList(21, 30).map { SimilarShow(show.id, it.id, 3) })
        db.showDao().insertSimilar(similar)

        assert(db.showDao().latestSimilarShow(show.id)!!.page == 3)
    }

    @Test
    fun testGetShow() = runBlocking {
        val shows: List<TvShow> = (1..10).map {
            TvShow(
                it.toLong(),
                "Show $it",
                voteAvg = 0.0,
                imgUrl = null,
                overview = null,
                originalLanguage = "en"
            )
        }
        db.showDao().insertAll(shows)
        assert(db.showDao().getTvShow(1)!!.id.toInt() == 1)
    }

    @Test
    fun testDeleteSimilarShows() = runBlocking {
        val shows: List<TvShow> = (1..50).map {
            TvShow(
                it.toLong(),
                "Show $it",
                voteAvg = 0.0,
                imgUrl = null,
                overview = null,
                originalLanguage = "en"
            )
        }
        db.showDao().insertAll(shows)
        val similar = mutableListOf<SimilarShow>()

        val show = shows.last()

        similar.addAll(shows.subList(0, 10).map { SimilarShow(show.id, it.id, 1) })
        similar.addAll(shows.subList(11, 20).map { SimilarShow(show.id, it.id, 2) })
        similar.addAll(shows.subList(21, 30).map { SimilarShow(show.id, it.id, 3) })
        db.showDao().insertSimilar(similar)

        val rowsAffected = db.showDao().deleteSimilarShows(show.id)
        assertEquals(similar.size, rowsAffected)
    }

    @Test
    fun testDeleteSimilarShowsOnCachedPage() = runBlocking {
        val shows: List<TvShow> = (1..50).map {
            TvShow(
                it.toLong(),
                "Show $it",
                voteAvg = 0.0,
                imgUrl = null,
                overview = null,
                originalLanguage = "en"
            )
        }
        db.showDao().insertAll(shows)
        val similar = mutableListOf<SimilarShow>()
        val show = shows.last()
        similar.addAll(shows.subList(0, 10).map { SimilarShow(show.id, it.id, 0) })
        db.showDao().insertSimilar(similar)
        val rowsAffected = db.showDao().deleteSimilarShowsCachedOnPageZero(show.id)
        assertEquals(similar.size, rowsAffected)
    }

    @Test
    fun testGetSimilarShowsPage() = runBlocking {
        val shows: List<TvShow> = (1..50).map {
            TvShow(
                it.toLong(),
                "Show $it",
                voteAvg = 0.0,
                imgUrl = null,
                overview = null,
                originalLanguage = "en"
            )
        }
        db.showDao().insertAll(shows)
        val similar = mutableListOf<SimilarShow>()
        val show = shows.last()
        similar.addAll(shows.subList(0, 10).map { SimilarShow(show.id, it.id, 3) })
        db.showDao().insertSimilar(similar)
        val results = db.showDao().getSimilarShowsPage(show.id, 3)
        assertEquals(similar, results)
    }


}