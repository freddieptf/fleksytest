package com.freddieptf.fleksytest.ui

import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import androidx.test.espresso.Espresso
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.platform.app.InstrumentationRegistry
import com.freddieptf.fleksytest.R
import com.freddieptf.fleksytest.Utils
import com.freddieptf.fleksytest.data.DataRepository
import com.freddieptf.fleksytest.data.MockApi
import com.freddieptf.fleksytest.data.db.TvShowDb
import com.freddieptf.fleksytest.data.db.model.TvShow
import com.freddieptf.fleksytest.di.DataModule
import com.freddieptf.fleksytest.ui.shows.PopularTvShowsFragment
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.UninstallModules
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test

@HiltAndroidTest
@UninstallModules(DataModule::class)
class PopularTvShowsFragmentTest {

    @Module
    @InstallIn(SingletonComponent::class)
    class FakeDataModule {
        @Provides
        fun provideDataRepository(): DataRepository = DataRepository(
            MockApi().apply {
                val shows: List<TvShow> = (1..DataRepository.PAGE_SIZE).map {
                    TvShow(
                        it.toLong(),
                        "Show $it",
                        voteAvg = 10.minus(it).toDouble(),
                        imgUrl = null,
                        overview = null,
                        originalLanguage = "lang-$it"
                    )
                }
                topRatedShows.addAll(shows)
            },
            Room.inMemoryDatabaseBuilder(
                InstrumentationRegistry.getInstrumentation().targetContext,
                TvShowDb::class.java
            ).build()
        )
    }

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Test
    fun loadsPopularTvShows() {
        runBlocking {
            Utils.launchFragmentInHiltContainer<PopularTvShowsFragment>()
            delay(1000) // flaky should use idling resource
            Espresso.onView(ViewMatchers.withId(R.id.recycler_popularShows))
                .check { view, noViewFoundException ->
                    noViewFoundException?.let { throw noViewFoundException }
                    val recyclerView = view as RecyclerView
                    val adapter = recyclerView.adapter!!
                    assertEquals(DataRepository.PAGE_SIZE, adapter.itemCount)
                }
        }
    }

}