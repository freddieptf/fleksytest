package com.freddieptf.fleksytest

interface ActivityWithToolBarProgress {
    fun showToolBarProgress(show: Boolean)
}