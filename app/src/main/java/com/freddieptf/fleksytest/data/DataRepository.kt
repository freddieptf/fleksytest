package com.freddieptf.fleksytest.data

import androidx.paging.ExperimentalPagingApi
import androidx.paging.Pager
import androidx.paging.PagingConfig
import com.freddieptf.fleksytest.data.api.TmDbApiService
import com.freddieptf.fleksytest.data.db.TvShowDb

@OptIn(ExperimentalPagingApi::class)
class DataRepository(val api: TmDbApiService, val db: TvShowDb) {

    companion object {
        val PAGE_SIZE: Int = 20
    }

    fun getPopularTvShows() = Pager(
        config = PagingConfig(pageSize = PAGE_SIZE, prefetchDistance = 60),
        remoteMediator = PopularShowRemoteMediator(api, db)
    ) {
        db.showDao().popularShows()
    }.flow

    fun getSimilarTvShows(showId: Long, originalLanguage: String) = Pager(
        config = PagingConfig(pageSize = PAGE_SIZE, prefetchDistance = 10, initialLoadSize = 10),
        remoteMediator = SimilarShowRemoteMediator(
            query = showId,
            api = api,
            db = db
        )
    ) {
        db.showDao().similarShows(showId, originalLanguage)
    }.flow

    suspend fun getTvShow(id: Long) = db.showDao().getTvShow(id)

}