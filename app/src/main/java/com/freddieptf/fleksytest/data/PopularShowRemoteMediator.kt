package com.freddieptf.fleksytest.data

import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import androidx.room.withTransaction
import com.freddieptf.fleksytest.data.api.TmDbApiService
import com.freddieptf.fleksytest.data.db.TvShowDb
import com.freddieptf.fleksytest.data.db.model.PopularShow
import com.freddieptf.fleksytest.data.db.model.TvShow

@ExperimentalPagingApi
class PopularShowRemoteMediator(
    val api: TmDbApiService,
    val db: TvShowDb
) : RemoteMediator<Int, TvShow>() {

    override suspend fun load(loadType: LoadType, state: PagingState<Int, TvShow>): MediatorResult {
        try {
            val loadKey = when (loadType) {
                LoadType.REFRESH -> null
                LoadType.PREPEND -> return MediatorResult.Success(endOfPaginationReached = true)
                LoadType.APPEND -> {
                    val item = db.showDao().lowestRatedShowItem()
                    item?.page?.plus(1)
                }
            }
            val response = api.getTopRatedShows(loadKey ?: 1)
            db.withTransaction {
                if (loadType == LoadType.REFRESH) db.showDao().deletePopular()
                db.showDao().insertAll(response.results)
                db.showDao().insertPopular(response.results.map {
                    PopularShow(
                        it.id,
                        it.voteAvg,
                        response.page
                    )
                })
            }
            return MediatorResult.Success(
                endOfPaginationReached = response.page == response.totalPages
            )
        } catch (e: Exception) {
            e.printStackTrace()
            return MediatorResult.Error(e)
        }
    }
}