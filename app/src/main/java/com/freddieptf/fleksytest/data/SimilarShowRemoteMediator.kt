package com.freddieptf.fleksytest.data

import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import androidx.room.withTransaction
import com.freddieptf.fleksytest.data.api.TmDbApiService
import com.freddieptf.fleksytest.data.db.TvShowDb
import com.freddieptf.fleksytest.data.db.model.SimilarShow
import com.freddieptf.fleksytest.data.db.model.TvShow

@ExperimentalPagingApi
class SimilarShowRemoteMediator(
    private val query: Long,
    private val api: TmDbApiService,
    private val db: TvShowDb
) : RemoteMediator<Int, TvShow>() {

    override suspend fun load(loadType: LoadType, state: PagingState<Int, TvShow>): MediatorResult {
        try {
            // we'll always have the show itself in the similar results at page 0
            db.withTransaction {
                if (loadType == LoadType.REFRESH) {
                    val show = db.showDao().getTvShow(query)!!
                    db.showDao().insertSimilar(
                        listOf(
                            SimilarShow(
                                showId = show.id,
                                similarShowId = show.id,
                                page = -1
                            )
                        )
                    )
                }
            }
            val loadKey = when (loadType) {
                LoadType.REFRESH -> {
                    db.withTransaction {
                        val firstCachedPage = db.showDao().getSimilarShowsPage(query, 1)
                        db.showDao()
                            .deleteSimilarShows(query) // deletes everything that's on page > 0
                        db.showDao()
                            .insertSimilar(firstCachedPage.map { it.copy(page = 0) }) // load some cached results into page zero so we atleast have something to show on the UI if append fails
                    }
                    return MediatorResult.Success(endOfPaginationReached = false)
                }
                LoadType.PREPEND -> return MediatorResult.Success(endOfPaginationReached = true)
                LoadType.APPEND -> db.showDao().latestSimilarShow(query)?.page?.plus(1)
            }
            val response = api.getSimilarShows(query, loadKey ?: 1)
            db.withTransaction {
                // since we are here, it means we got some fresh data
                db.showDao().deleteSimilarShowsCachedOnPageZero(query)
                db.showDao().insertAll(response.results)
                db.showDao().insertSimilar(response.results.map {
                    SimilarShow(
                        showId = query,
                        similarShowId = it.id,
                        response.page
                    )
                })
            }

            return MediatorResult.Success(
                endOfPaginationReached = response.page == response.totalPages
            )
        } catch (e: Exception) {
            e.printStackTrace()
            return MediatorResult.Error(e)
        }
    }
}