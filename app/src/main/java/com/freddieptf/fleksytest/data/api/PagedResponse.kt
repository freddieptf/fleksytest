package com.freddieptf.fleksytest.data.api

import com.google.gson.annotations.SerializedName

data class PagedResponse<T>(
    val page: Int,
    val results: List<T>,
    @SerializedName("total_pages") val totalPages: Int,
    @SerializedName("total_results") val totalResults: Int
)