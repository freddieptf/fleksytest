package com.freddieptf.fleksytest.data.api

import com.freddieptf.fleksytest.data.db.model.TvShow
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface TmDbApiService {
    @GET("tv/top_rated")
    suspend fun getTopRatedShows(@Query("page") page: Int): PagedResponse<TvShow>

    @GET("tv/{showId}/similar")
    suspend fun getSimilarShows(
        @Path("showId") showId: Long,
        @Query("page") page: Int
    ): PagedResponse<TvShow>
}