package com.freddieptf.fleksytest.data.api

import com.freddieptf.fleksytest.BuildConfig
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

/**
adds api_key query param to all requests
 * */
class TmdbApiKeyInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val url = request.url.newBuilder().addQueryParameter("api_key", BuildConfig.API_ACCESS_KEY)
            .build()
        val requestBuilder: Request.Builder = request.newBuilder()
            .url(url)
        return chain.proceed(requestBuilder.build())
    }
}