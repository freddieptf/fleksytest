package com.freddieptf.fleksytest.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.freddieptf.fleksytest.data.db.dao.TvShowDao
import com.freddieptf.fleksytest.data.db.model.PopularShow
import com.freddieptf.fleksytest.data.db.model.SimilarShow
import com.freddieptf.fleksytest.data.db.model.TvShow

@Database(entities = [TvShow::class, PopularShow::class, SimilarShow::class], version = 1)
abstract class TvShowDb : RoomDatabase() {
    abstract fun showDao(): TvShowDao
}