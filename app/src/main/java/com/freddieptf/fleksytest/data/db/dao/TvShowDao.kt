package com.freddieptf.fleksytest.data.db.dao

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.freddieptf.fleksytest.data.db.model.PopularShow
import com.freddieptf.fleksytest.data.db.model.SimilarShow
import com.freddieptf.fleksytest.data.db.model.TvShow

@Dao
interface TvShowDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(shows: List<TvShow>)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertPopular(shows: List<PopularShow>)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertSimilar(shows: List<SimilarShow>)

    @Query("SELECT s.id, s.title, s.overview, s.imgUrl, s.voteAvg, s.originalLanguage FROM popular_shows p JOIN shows s on p.showId=s.id ORDER BY p.voteAvg DESC, p.page ASC")
    fun popularShows(): PagingSource<Int, TvShow>

    @Query("SELECT s.id, s.title, s.overview, s.imgUrl, s.voteAvg, s.originalLanguage FROM similar_shows ss JOIN shows s on ss.similarShowId=s.id WHERE ss.showId=:showId AND s.originalLanguage=:originalLanguage ORDER BY ss.page ASC")
    fun similarShows(showId: Long, originalLanguage: String): PagingSource<Int, TvShow>

    @Query("SELECT * FROM similar_shows WHERE showId=:showId AND page=:page")
    fun getSimilarShowsPage(showId: Long, page: Int): List<SimilarShow>

    @Query("DELETE FROM similar_shows WHERE showId=:showId AND similarShowId!=:showId AND page < 1")
    fun deleteSimilarShowsCachedOnPageZero(showId: Long): Int

    @Query("SELECT * FROM popular_shows ORDER BY voteAvg ASC, page DESC LIMIT 1")
    suspend fun lowestRatedShowItem(): PopularShow?

    @Query("SELECT * FROM similar_shows WHERE showId=:showId AND page > 0 ORDER BY page DESC LIMIT 1")
    suspend fun latestSimilarShow(showId: Long): SimilarShow?

    @Query("SELECT * FROM shows WHERE id=:id")
    suspend fun getTvShow(id: Long): TvShow?

    @Query("DELETE FROM popular_shows")
    suspend fun deletePopular(): Int

    @Query("DELETE FROM similar_shows WHERE showId=:showId AND page > 0")
    suspend fun deleteSimilarShows(showId: Long): Int
}