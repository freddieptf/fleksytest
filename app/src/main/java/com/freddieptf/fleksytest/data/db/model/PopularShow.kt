package com.freddieptf.fleksytest.data.db.model

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(
    tableName = "popular_shows",
    foreignKeys = [ForeignKey(
        entity = TvShow::class,
        parentColumns = ["id"],
        childColumns = ["showId"]
    )]
)
data class PopularShow(
    @PrimaryKey val showId: Long,
    val voteAvg: Double?,
    val page: Int
)