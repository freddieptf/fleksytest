package com.freddieptf.fleksytest.data.db.model

import androidx.room.Entity
import androidx.room.ForeignKey

@Entity(
    tableName = "similar_shows",
    foreignKeys = [
        ForeignKey(
            entity = TvShow::class,
            parentColumns = ["id"],
            childColumns = ["showId"]
        ),
        ForeignKey(
            entity = TvShow::class,
            parentColumns = ["id"],
            childColumns = ["similarShowId"]
        )],
    primaryKeys = ["showId", "similarShowId"]
)
data class SimilarShow(
    val showId: Long,
    val similarShowId: Long,
    val page: Int
)