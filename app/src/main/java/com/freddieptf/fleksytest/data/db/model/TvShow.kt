package com.freddieptf.fleksytest.data.db.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "shows")
data class TvShow(
    @PrimaryKey @SerializedName("id") val id: Long,
    @SerializedName("name") val title: String,
    @SerializedName("overview") val overview: String?,
    @SerializedName("poster_path") val imgUrl: String?,
    @SerializedName("vote_average") val voteAvg: Double?,
    @SerializedName("original_language") val originalLanguage: String
)