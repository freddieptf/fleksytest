package com.freddieptf.fleksytest.di

import android.content.Context
import androidx.room.Room
import com.freddieptf.fleksytest.BuildConfig
import com.freddieptf.fleksytest.data.DataRepository
import com.freddieptf.fleksytest.data.api.TmDbApiService
import com.freddieptf.fleksytest.data.api.TmdbApiKeyInterceptor
import com.freddieptf.fleksytest.data.db.TvShowDb
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
@InstallIn(SingletonComponent::class)
class DataModule {

    @Provides
    fun providesAuthInterceptor(): TmdbApiKeyInterceptor = TmdbApiKeyInterceptor()

    @Provides
    fun provideTmdbApiHttpClient(authInterceptor: TmdbApiKeyInterceptor): OkHttpClient {
        return OkHttpClient.Builder()
            .addNetworkInterceptor(authInterceptor)
            .addInterceptor(HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            })
            .build()
    }

    @Provides
    fun provideTmDbApiService(okHttpClient: OkHttpClient): TmDbApiService =
        Retrofit.Builder()
            .baseUrl(BuildConfig.API_BACKEND)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(TmDbApiService::class.java)

    @Provides
    fun provideDb(@ApplicationContext context: Context): TvShowDb =
        Room.databaseBuilder(context, TvShowDb::class.java, "tmdb")
            .fallbackToDestructiveMigration()
            .build()

    @Provides
    fun provideDataRepository(api: TmDbApiService, db: TvShowDb): DataRepository =
        DataRepository(api, db)

}