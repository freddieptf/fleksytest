package com.freddieptf.fleksytest.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.cachedIn
import com.freddieptf.fleksytest.data.DataRepository
import com.freddieptf.fleksytest.data.db.model.TvShow
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class ShowsViewModel @Inject constructor(private val dataRepository: DataRepository) : ViewModel() {

    val popularTvShows = dataRepository.getPopularTvShows().cachedIn(viewModelScope)

    fun getSimilarShows(id: Long, originalLanguage: String) =
        dataRepository.getSimilarTvShows(id, originalLanguage)

    suspend fun getTvShow(id: Long): Pair<TvShow?, IllegalStateException?> {
        val show: TvShow?
        withContext(Dispatchers.IO) {
            show = dataRepository.getTvShow(id)
        }
        show?.let { return it to null }
        return null to IllegalStateException("Show not found")
    }
}