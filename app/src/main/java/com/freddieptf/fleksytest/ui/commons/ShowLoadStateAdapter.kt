package com.freddieptf.fleksytest.ui.commons

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import androidx.annotation.LayoutRes
import androidx.annotation.StringRes
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.recyclerview.widget.RecyclerView
import com.freddieptf.fleksytest.R

class ShowLoadStateViewHolder(
    itemView: View,
    @StringRes val errorString: Int,
    retry: () -> Unit
) : RecyclerView.ViewHolder(itemView) {

    private val retryButton = itemView.findViewById<Button>(R.id.btn_load_retry_button)
    private val progressBar = itemView.findViewById<ProgressBar>(R.id.pb_load_progress_bar)
    private val errorText = itemView.findViewById<TextView>(R.id.tv_load_error_msg)

    init {
        retryButton.setOnClickListener { retry.invoke() }
    }

    fun bind(loadState: LoadState) {
        if (loadState is LoadState.Error) {
            errorText.text =
                itemView.context.getString(errorString, loadState.error.localizedMessage ?: "")
        }
        progressBar.isVisible = loadState is LoadState.Loading
        retryButton.isVisible = loadState is LoadState.Error
        errorText.isVisible = loadState is LoadState.Error
    }
}


class ShowsLoadStateAdapter(
    @LayoutRes private val layoutId: Int,
    @StringRes private val errorString: Int,
    private val retry: () -> Unit
) :
    LoadStateAdapter<ShowLoadStateViewHolder>() {
    override fun onBindViewHolder(holder: ShowLoadStateViewHolder, loadState: LoadState) {
        holder.bind(loadState)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        loadState: LoadState
    ): ShowLoadStateViewHolder {
        return ShowLoadStateViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(layoutId, parent, false), errorString, retry
        )
    }
}
