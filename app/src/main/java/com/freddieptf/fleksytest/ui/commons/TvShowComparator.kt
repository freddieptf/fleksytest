package com.freddieptf.fleksytest.ui.commons

import androidx.recyclerview.widget.DiffUtil
import com.freddieptf.fleksytest.data.db.model.TvShow

class TvShowComparator : DiffUtil.ItemCallback<TvShow>() {
    override fun areItemsTheSame(oldItem: TvShow, newItem: TvShow): Boolean =
        oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: TvShow, newItem: TvShow): Boolean =
        oldItem == newItem
}