package com.freddieptf.fleksytest.ui.detail

import android.text.method.ScrollingMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.freddieptf.fleksytest.R
import com.freddieptf.fleksytest.data.GlideApp
import com.freddieptf.fleksytest.data.db.model.TvShow
import com.freddieptf.fleksytest.databinding.ListItemTvShowDetailBinding

class SimilarShowViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    private val binding = ListItemTvShowDetailBinding.bind(itemView)
    fun bind(tvShow: TvShow?) {
        binding.tvTvShowDetailTitle.text = "Loading"
        tvShow?.let {
            it.imgUrl?.let { url ->
                GlideApp.with(itemView)
                    .load("https://image.tmdb.org/t/p/w342" + url)
                    .centerCrop()
                    .into(binding.imageTvShowDetailHero)
            }
            binding.tvTvShowDetailTitle.text = it.title
            binding.tvTvShowDetailOverview.text = it.overview
            binding.tvTvShowDetailOverview.movementMethod = ScrollingMovementMethod()
        }
    }
}

class SimilarShowsAdapter(diffCallback: DiffUtil.ItemCallback<TvShow>) :
    PagingDataAdapter<TvShow, SimilarShowViewHolder>(diffCallback) {
    override fun onBindViewHolder(holder: SimilarShowViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimilarShowViewHolder {
        return SimilarShowViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item_tv_show_detail, parent, false)
        )
    }
}