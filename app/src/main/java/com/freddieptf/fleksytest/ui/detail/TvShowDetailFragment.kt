package com.freddieptf.fleksytest.ui.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import com.freddieptf.fleksytest.R
import com.freddieptf.fleksytest.databinding.FragmentTvShowDetailBinding
import com.freddieptf.fleksytest.ui.ShowsViewModel
import com.freddieptf.fleksytest.ui.commons.ShowsLoadStateAdapter
import com.freddieptf.fleksytest.ui.commons.TvShowComparator
import com.google.android.material.snackbar.BaseTransientBottomBar.LENGTH_LONG
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
@AndroidEntryPoint
class TvShowDetailFragment : Fragment() {

    private var _binding: FragmentTvShowDetailBinding? = null
    private val binding get() = _binding!!
    private val viewModel by activityViewModels<ShowsViewModel>()
    private var adapter = SimilarShowsAdapter(TvShowComparator())
    private var snackbar: Snackbar? = null

    private val TAG = "TvShowDetailFragment"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentTvShowDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.recyclerDetailShow.itemAnimator = null
        binding.recyclerDetailShow.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        PagerSnapHelper().attachToRecyclerView(binding.recyclerDetailShow)
        binding.recyclerDetailShow.adapter = adapter.withLoadStateFooter(
            ShowsLoadStateAdapter(
                R.layout.footer_view_similar_shows,
                R.string.error_load_similar
            ) { adapter.retry() }
        )
        binding.retryButton.setOnClickListener { adapter.retry() }

        with(viewLifecycleOwner.lifecycleScope) {
            launch {
                viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                    val result = viewModel.getTvShow(requireArguments().getLong("show_id"))
                    if (result.second != null) {
                        showLoadError(result.second.toString(), {})
                        return@repeatOnLifecycle
                    }
                    val show = result.first!!
                    viewModel.getSimilarShows(show.id, show.originalLanguage).collect {
                        adapter.submitData(it)
                    }
                }
            }

            launch {
                viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                    adapter.loadStateFlow.collect { loadState ->
                        binding.progressBar.isVisible = loadState.refresh is LoadState.Loading
                        binding.errorStateParent.isVisible =
                            loadState.refresh is LoadState.Error && adapter.itemCount == 0
                        val errorState = loadState.refresh as? LoadState.Error
                        errorState?.let {
                            showLoadError(
                                getString(
                                    R.string.error_refresh,
                                    it.error.localizedMessage ?: ""
                                ), adapter::refresh
                            )
                        }
                    }

                }
            }
        }
    }

    fun showLoadError(error: String, retry: () -> Unit) {
        snackbar = Snackbar.make(binding.recyclerDetailShow, error, LENGTH_LONG)
            .setAction(R.string.retry) {
                retry.invoke()
            }
        snackbar!!.show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        snackbar?.dismiss()
        _binding = null
    }
}