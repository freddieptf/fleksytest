package com.freddieptf.fleksytest.ui.shows

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.freddieptf.fleksytest.R
import com.freddieptf.fleksytest.data.GlideApp
import com.freddieptf.fleksytest.data.db.model.TvShow
import com.freddieptf.fleksytest.databinding.ListItemTvShowBinding

class ShowViewHolder(itemView: View, val callback: (TvShow?) -> Unit) :
    RecyclerView.ViewHolder(itemView), View.OnClickListener {
    private val binding = ListItemTvShowBinding.bind(itemView)
    private var _tvShow: TvShow? = null

    init {
        binding.root.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        callback(_tvShow)
    }

    fun bind(tvShow: TvShow?) {
        _tvShow = tvShow
        binding.tvTvShowTitle.text = "Loading"
        tvShow?.let {
            it.imgUrl?.let { url ->
                GlideApp.with(itemView)
                    .load("https://image.tmdb.org/t/p/w342" + url)
                    .centerCrop()
                    .into(binding.imageTvShowImage)
            }
            binding.tvTvShowTitle.text = it.title
            binding.tvTvShowVoteAvg.text = itemView.context.getString(R.string.rating, it.voteAvg)
        }
    }
}

class ShowsAdapter(
    diffCallback: DiffUtil.ItemCallback<TvShow>,
    val clickCallback: (TvShow?) -> Unit
) :
    PagingDataAdapter<TvShow, ShowViewHolder>(diffCallback) {
    override fun onBindViewHolder(holder: ShowViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShowViewHolder =
        ShowViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.list_item_tv_show, parent, false),
            clickCallback
        )
}