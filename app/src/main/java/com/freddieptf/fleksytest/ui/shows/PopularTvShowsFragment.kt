package com.freddieptf.fleksytest.ui.shows

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import com.freddieptf.fleksytest.ActivityWithToolBarProgress
import com.freddieptf.fleksytest.R
import com.freddieptf.fleksytest.databinding.FragmentTvShowsBinding
import com.freddieptf.fleksytest.ui.ShowsViewModel
import com.freddieptf.fleksytest.ui.commons.TvShowComparator
import com.google.android.material.snackbar.BaseTransientBottomBar.LENGTH_INDEFINITE
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
@AndroidEntryPoint
@OptIn(ExperimentalPagingApi::class)
class PopularTvShowsFragment : Fragment() {

    private var _binding: FragmentTvShowsBinding? = null
    private val binding get() = _binding!!
    private val viewModel by activityViewModels<ShowsViewModel>()
    private var snackbarState: Pair<Snackbar, String>? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentTvShowsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.recyclerPopularShows.itemAnimator = null
        val layoutManager = GridLayoutManager(requireContext(), 2)
        binding.recyclerPopularShows.layoutManager = layoutManager

        val adapter = ShowsAdapter(TvShowComparator()) { show ->
            with(findNavController()) {
                navigate(
                    R.id.action_openDetailView,
                    bundleOf("show_id" to (show?.id ?: -1)),
                )
            }
        }
        binding.recyclerPopularShows.adapter = adapter
        binding.retryButton.setOnClickListener { adapter.retry() }

        with(viewLifecycleOwner.lifecycleScope) {
            launch {
                viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                    viewModel.popularTvShows.collect {
                        adapter.submitData(it)
                    }
                }
            }
            launch {
                viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                    adapter.loadStateFlow.collect { loadState ->
                        binding.progressBar.isVisible = loadState.refresh is LoadState.Loading
                        binding.errorStateParent.isVisible =
                            loadState.refresh is LoadState.Error && adapter.itemCount == 0
                        val errorState = loadState.refresh as? LoadState.Error
                        errorState?.let {
                            showRetryableError(
                                getString(
                                    R.string.error_refresh,
                                    it.error.localizedMessage ?: ""
                                ), adapter::refresh
                            )
                        }
                        if (requireActivity() is ActivityWithToolBarProgress) {
                            (requireActivity() as ActivityWithToolBarProgress).showToolBarProgress(
                                loadState.append is LoadState.Loading
                            )
                        }
                        (loadState.append as? LoadState.Error)?.let {
                            showRetryableError(
                                getString(
                                    R.string.error_load_more,
                                    it.error.localizedMessage ?: ""
                                ), adapter::retry
                            )
                        }
                    }
                }
            }
        }
    }

    private fun showRetryableError(error: String, retry: () -> Unit) {
        if (snackbarState == null || snackbarState!!.second != error) {
            snackbarState?.first?.dismiss()
            val snackbar = Snackbar.make(binding.recyclerPopularShows, error, LENGTH_INDEFINITE)
                .setAction(R.string.retry) {
                    retry.invoke()
                }
            snackbarState = snackbar to error
            snackbar.show()
            return
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        snackbarState?.first?.dismiss()
        _binding = null
    }
}