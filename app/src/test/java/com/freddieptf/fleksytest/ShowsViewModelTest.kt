package com.freddieptf.fleksytest.ui

import androidx.paging.AsyncPagingDataDiffer
import androidx.paging.PagingData
import androidx.recyclerview.widget.ListUpdateCallback
import com.freddieptf.fleksytest.data.DataRepository
import com.freddieptf.fleksytest.data.db.model.TvShow
import com.freddieptf.fleksytest.ui.commons.TvShowComparator
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNull
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock

class NoopListCallback : ListUpdateCallback {
    override fun onChanged(position: Int, count: Int, payload: Any?) {}
    override fun onMoved(fromPosition: Int, toPosition: Int) {}
    override fun onInserted(position: Int, count: Int) {}
    override fun onRemoved(position: Int, count: Int) {}
}

@ExperimentalCoroutinesApi
class ShowsViewModelTest {

    @Test
    fun testGetPopular() = runBlocking {
        val shows = (1..3).map {
            TvShow(
                it.toLong(),
                "Show $it",
                voteAvg = 10.minus(it).toDouble(),
                imgUrl = null,
                overview = null,
                originalLanguage = "lang-$it"
            )
        }
        val dataRepository = mock<DataRepository> {
            onBlocking {
                getPopularTvShows()
            } doReturn flow {
                emit(PagingData.from(shows))
            }
        }
        val viewModel = ShowsViewModel(dataRepository)
        val differ = AsyncPagingDataDiffer<TvShow>(
            TvShowComparator(),
            NoopListCallback(),
            Dispatchers.Default
        )
        differ.submitData(viewModel.popularTvShows.first())
        assertEquals(shows, differ.snapshot().items)
    }

    @Test
    fun testGetSimilar() = runBlocking {
        val shows = (1..3).map {
            TvShow(
                it.toLong(),
                "Show $it",
                voteAvg = 10.minus(it).toDouble(),
                imgUrl = null,
                overview = null,
                originalLanguage = "lang-$it"
            )
        }
        val dataRepository = mock<DataRepository> {
            onBlocking {
                getSimilarTvShows(-1, "en")
            } doReturn flow {
                emit(PagingData.from(shows))
            }
        }
        val viewModel = ShowsViewModel(dataRepository)
        val differ = AsyncPagingDataDiffer<TvShow>(
            TvShowComparator(),
            NoopListCallback(),
            Dispatchers.Default
        )
        differ.submitData(viewModel.getSimilarShows(-1, "en").first())
        assertEquals(shows, differ.snapshot().items)
    }

    @Test
    fun testGetShow() = runBlocking {
        val show = TvShow(
            1,
            "Show",
            voteAvg = 10.0,
            imgUrl = null,
            overview = null,
            originalLanguage = "lang-en"
        )
        val dataRepository = mock<DataRepository> {
            onBlocking {
                getTvShow(1)
            } doReturn show
        }
        val viewModel = ShowsViewModel(dataRepository)
        val result = viewModel.getTvShow(1)
        assertNull(result.second)
        assert(result.first == show)
    }
}